package agh.cs.projekt2;

import com.google.gson.internal.LinkedTreeMap;

import java.util.Comparator;
import java.util.List;

public class PolishParlimentMP implements Person{

    private int ID;
    private String fullName;
    private List<Expense> expenses = null;
    private List<Trip> trips = null;

    PolishParlimentMP(int ID, String fullName) {
        this.ID = ID;
        this.fullName = fullName;
    }

    PolishParlimentMP(LinkedTreeMap<String, LinkedTreeMap> json) {
        LinkedTreeMap<String, String> personData = json.get("data");
        this.ID = Integer.parseInt(personData.get("poslowie.id"));
        this.fullName = personData.get("poslowie.nazwa");
    }

    @Override
    public String toString() {
        return this.fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof PolishParlimentMP) {
            PolishParlimentMP object = (PolishParlimentMP) o;
            return object.fullName.equals(this.fullName) && object.getID() == this.getID();
        }
        return false;
    }

    public int getID() {
        return this.ID;
    }

    public String getFullName() {
        return this.fullName;
    }

    public List<Trip> getTrips() {
        return this.trips;
    }

    public double getExpensesSum(String reason) {
        if (this.expenses.size() == 0)
            return 0.0;
        if (reason.equals("all"))
            return this.expenses.stream()
                    .map(Expense::getPrice)
                    .reduce(0.0, (a, b) -> a + b);
        else
            return this.expenses.stream()
                    .filter(expense -> expense.getPurpose().equals(reason))
                    .map(Expense::getPrice)
                    .reduce(0.0, (a, b) -> a + b);
    }

    public Trip getTopCostTrip() {
        if (this.trips.size() == 0)
            return new PolishMPTrip(0.0);
        else
            return this.trips.stream()
                    .max(Comparator.comparingDouble(Trip::getCost))
                    .orElse(new PolishMPTrip(0.0));
    }

    public int getTripsTotalTime() {
        return this.trips.stream()
                .map(Trip::getDuration)
                .reduce(0, (a, b) -> a + b);
    }

    public boolean hasVisited(String country) {
        return this.trips.stream()
                .anyMatch(destination -> destination.getDestination().equals(country));
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

}
