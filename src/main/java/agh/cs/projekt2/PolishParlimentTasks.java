package agh.cs.projekt2;

/**
 * Created by macho_000 on 2017-01-20.
 */
public enum PolishParlimentTasks {
    ExpensesSum,
    MinorExpensesSum,
    AverageExpenses,
    TopTravels,
    TopTimeTravel,
    TopCostTravel,
    Italy
}
