package agh.cs.projekt2;

/**
 * Created by macho_000 on 11.01.2017.
 */
public interface Trip {
    String getDestination();
    int getDuration();
    double getCost();
}
