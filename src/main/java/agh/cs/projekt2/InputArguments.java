package agh.cs.projekt2;

public class InputArguments {
    private final String mpName;
    private final int term;
    private final PolishParlimentTasks task;

    InputArguments(String mpName, int term, PolishParlimentTasks task) {
        this.mpName = mpName;
        this.term = term;
        this.task = task;
    }

    public String getMPName() {
        return this.mpName;
    }

    public int getTerm() {
        return this.term;
    }

    public PolishParlimentTasks getTask() {
        return this.task;
    }
}
