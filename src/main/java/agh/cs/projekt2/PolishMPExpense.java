package agh.cs.projekt2;

public class PolishMPExpense implements Expense {

    private final double cost;
    private final String reason;

    PolishMPExpense(double cost, String reason) {
        this.cost = cost;
        this.reason = reason;
    }

    @Override
    public String toString() {
        return this.cost + " reason: " + this.reason;
    }

    @Override
    public double getPrice() {
        return this.cost;
    }

    @Override
    public String getPurpose() {
        return this.reason;
    }
}
