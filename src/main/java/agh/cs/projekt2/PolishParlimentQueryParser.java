package agh.cs.projekt2;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class PolishParlimentQueryParser implements QueryParser {

    private static final int firstTerm = 7;
    private static final int lastTerm = 8;
    private static final String tripsToken = "wyjazdy";
    private static final String expensesToken = "wydatki";
    private static final String repairReason = "Koszty drobnych napraw i remontów lokalu biura poselskiego";
    private static final String country = "Włochy";

    private static APIConnector accessor = new APILink();
    private List<Person> mps = new ArrayList<>();

    PolishParlimentQueryParser(int termNumber) throws IOException {
        mps = accessor.getDeputies(termNumber);
    }

    PolishParlimentQueryParser(String mpName, int term, PolishParlimentTasks task) throws IllegalArgumentException, IOException {
        if (term > lastTerm && term < firstTerm)
            throw new IllegalArgumentException("Available terms: 7, 8");

        StringBuilder result = new StringBuilder();
        DecimalFormat df = new DecimalFormat("0.00");
        System.out.println("Getting list of MPs in term #" + term);
        mps = accessor.getDeputies(term);
        switch (task) {
            case ExpensesSum:
                System.out.println("Looking for expenses for " + mpName + " in term #" + term);
                if (getExpensesSum(mpName) > 0)
                    result.append("Total expenses of " +
                            mpName +
                            " in term #" +
                            term + ": " +
                            df.format(getExpensesSum(mpName)) + " zł" +
                            '\n'
                    );
                else
                    throw new IllegalArgumentException("No such PolishParlimentMP was found in parliament in term #" + term);
                break;
            case MinorExpensesSum:
                System.out.println("Looking for minor expenses (office repairs) for "
                        + mpName + " in term #" + term);
                if (getMinorExpensesSum(mpName) > 0)
                    result.append("Minor expenses of " +
                            mpName +
                            " in term #" +
                            term + ": " +
                            df.format(getMinorExpensesSum(mpName)) + " zł" + '\n');
                else
                    throw new IllegalArgumentException("No such PolishParlimentMP was found in parliament in term #" + term);
                break;
            case AverageExpenses:
                System.out.println("Looking for average expenses of PolishParlimentMP's in term #" + term);
                setMpsProperty(expensesToken);
                result.append("Average expenses of MPs in term #" +
                        term + ": " +
                        df.format(getAverageExpenses()) + " zł " + '\n');
                break;
            case TopTravels:
                System.out.println("Looking for PolishParlimentMP with largest number of trips in term #" + term);
                setMpsProperty(tripsToken);
                result.append(getMostTraveledMP().toString() +
                        " travelled " +
                        getMostTraveledMP().getTrips().size() +
                        " times in term #" +
                        term + '\n');
                break;
            case TopTimeTravel:
                System.out.println("Looking for PolishParlimentMP who spent most time abroad in term #" + term);
                setMpsProperty(tripsToken);
                result.append(getLongestTraveledMP().toString() +
                        " travelled " +
                        getLongestTraveledMP().getTripsTotalTime() +
                        " days in term #" +
                        term + '\n');
                break;
            case TopCostTravel:
                System.out.println("Looking for the PolishParlimentMP with most expensive trip in term #" + term);
                setMpsProperty(tripsToken);
                result.append(getMostLavishMP().toString() + " in term #" +
                        term + " on " +
                        getMostLavishMP().getTopCostTrip().toString() + '\n');
                break;
            case Italy:
                System.out.println("Looking for MPs who visited Italy in term #" + term);
                setMpsProperty(tripsToken);
                result.append("Following MPs visited Italy in term #" + term + ":\n");
                getCountryVisitors(country).forEach(mp -> result.append(mp.getFullName() + '\n'));
                break;
        }
        System.out.println(result.toString());
    }

    @Override
    public double getExpensesSum(String fullName) throws IOException {
        Person member = mps.stream()
                .filter(mp -> mp.getFullName().equals(fullName))
                .findFirst()
                .orElse(null);
        if (member == null)
            return 0.0;
        else
            member.setExpenses(accessor.getExpenses(member.getID()));
        return member.getExpensesSum("all");
    }

    @Override
    public double getMinorExpensesSum(String fullName) throws IOException {
        Person member = mps.stream()
                .filter(mp -> mp.getFullName().equals(fullName))
                .findFirst()
                .orElse(null);
        if (member == null)
            return 0.0;
        else
            member.setExpenses(accessor.getExpenses(member.getID()));
        return member.getExpensesSum(repairReason);
    }


    @Override
    public double getAverageExpenses() {
        return mps.stream().
                mapToDouble(mp -> mp.getExpensesSum("all")).
                sum()
                / mps.size();
    }

    @Override
    public Person getMostTraveledMP() {
        mps.sort(Comparator.comparingInt(mp -> mp.getTrips().size()));
        return mps.get(mps.size() - 1);
    }

    @Override
    public Person getLongestTraveledMP() {
        mps.sort(Comparator.comparingInt(Person::getTripsTotalTime));
        return mps.get(mps.size() - 1);
    }

    @Override
    public Person getMostLavishMP() {
        mps.sort(Comparator.comparingDouble(mp -> mp.getTopCostTrip().getCost()));
        return mps.get(mps.size() - 1);
    }

    @Override
    public List<Person> getCountryVisitors(String destinationCountry) {
        return mps.stream()
                .filter(mp -> mp.hasVisited(destinationCountry))
                .collect(Collectors.toList());
    }
    // obejscie wyjatkow przy setmpsproperty, brzydkie ale program dziala i jest bezpieczniej
    void setMpsProperty(String property) throws IOException, IllegalArgumentException {
        try {
            switch (property) {
                case expensesToken:
                    mps.parallelStream().forEach(mp -> {
                                try {
                                    mp.setExpenses(accessor.getExpenses(mp.getID()));
                                } catch (IOException ex) {
                                    throw new UncheckedIOException(ex);
                                }
                            }
                    );
                    break;
                case tripsToken:
                    mps.parallelStream().forEach(mp -> {
                                try {
                                    mp.setTrips(accessor.getTrips(mp.getID()));
                                } catch (IOException ex) {
                                    throw new UncheckedIOException(ex);
                                }
                            }
                    );
                    break;
                default:
                    throw new IllegalArgumentException("Incorrect property passed: " + property);
            }
        } catch (UncheckedIOException ex) {
            throw new IOException();
        }
    }

}
