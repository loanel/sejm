package agh.cs.projekt2;

import java.io.IOException;
import java.util.List;

/**
 * Created by macho_000 on 11.01.2017.
 */
public interface QueryParser {

    double getExpensesSum(String fullName) throws IOException;
    double getMinorExpensesSum(String fullname) throws IOException;
    double getAverageExpenses();

    Person getMostTraveledMP();
    Person getLongestTraveledMP();
    Person getMostLavishMP();

    List<Person> getCountryVisitors(String destinationCountry);



}
