package agh.cs.projekt2;


import java.text.DecimalFormat;
import java.time.LocalDate;

public class PolishMPTrip implements Trip {

    private final String country;
    private final LocalDate startDate;
    private final LocalDate endDate;
    private final int durationDays;
    private final double cost;

    PolishMPTrip(double cost) {
        this.country = null;
        this.startDate = null;
        this.endDate = null;
        this.durationDays = 0;
        this.cost = cost;
    }

    PolishMPTrip(String country, LocalDate start, LocalDate end, int days, double cost) {
        this.country = country;
        this.startDate = start;
        this.endDate = end;
        this.durationDays = days;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "trip to " + this.country + " from " + this.startDate + " to "
                + this.endDate + ". Cost: " + new DecimalFormat("0.00").format(this.cost) + " zł";
    }

    @Override
    public String getDestination() {
        return this.country;
    }

    @Override
    public int getDuration() {
        return this.durationDays;
        // Period.between(this.startDate, this.endDate).getDays();
    }

    @Override
    public double getCost() {
        return this.cost;
    }
}
