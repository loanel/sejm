package agh.cs.projekt2;

/*
Arguments that are valid for current PolishParlimentArgumentParser implementation :

    sum firstName lastName #term
    minor firstName lastName #term
    average #term
    top #term
    longest #term
    priciest #term
    italy #term

 */
import java.io.IOException;
import java.util.Arrays;

public class App 
{
    private static final String[] validArguments = new String[]{
            "sum firstName lastName #term",
            "minor firstName lastName #term",
            "average #term",
            "top #term",
            "longest #term",
            "priciest #term",
            "italy #term"};

    public static void main( String[] args ) {
        try {
            System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "50"); //http://stackoverflow.com/questions/21163108/custom-thread-pool-in-java-8-parallel-stream
            long startTime = System.nanoTime();
            InputArguments input = new PolishParlimentArgumentParser().parse(args);
            QueryParser processor = new PolishParlimentQueryParser(input.getMPName(), input.getTerm(), input.getTask());
            long endTime = System.nanoTime();
            System.out.println("Elapsed time: " + (endTime - startTime) / 1000000000 + "s");
        } catch (IOException ex) {
            System.err.println("Connection error. Try again later");
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
            System.err.println("Available options:");
            Arrays.stream(validArguments).forEach(System.out::println);
        } catch (Exception ex) {
            System.err.println("Another error occurred.");
        }
    }
}
