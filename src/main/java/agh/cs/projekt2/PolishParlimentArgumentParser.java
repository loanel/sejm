package agh.cs.projekt2;

import java.util.HashMap;
import java.util.Map;

public class PolishParlimentArgumentParser {

    private static Map<String, PolishParlimentTasks> argTypes = new HashMap<>();

    private static final String INCORRECT_NUMBER_OF_ARGUMENTS = "Enter correct number of arguments";
    private static final String INCORRECT_TASK = "Enter correct task";
    private static final String INCORRECT_NAME = "Enter correct PolishParlimentMP's name";
    private static final String INCORRECT_TERM = "Enter correct term number";

    PolishParlimentArgumentParser() {
        argTypes.put("sum", PolishParlimentTasks.ExpensesSum);
        argTypes.put("minor", PolishParlimentTasks.MinorExpensesSum);
        argTypes.put("average", PolishParlimentTasks.AverageExpenses);
        argTypes.put("top", PolishParlimentTasks.TopTravels);
        argTypes.put("longest", PolishParlimentTasks.TopTimeTravel);
        argTypes.put("priciest", PolishParlimentTasks.TopCostTravel);
        argTypes.put("italy", PolishParlimentTasks.Italy);
    }

    public InputArguments parse(String[] arguments) throws IllegalArgumentException {
        if (arguments.length < 2) {
            throw new IllegalArgumentException(INCORRECT_NUMBER_OF_ARGUMENTS);
        } else {
            PolishParlimentTasks submittedtask = argTypes.get(arguments[0]);
            if (submittedtask == null)
                throw new IllegalArgumentException(INCORRECT_TASK);
            if (!isPositive(arguments[arguments.length - 1]))
                throw new IllegalArgumentException(INCORRECT_TERM);
            if ((submittedtask == PolishParlimentTasks.ExpensesSum || submittedtask == PolishParlimentTasks.MinorExpensesSum) && arguments.length < 4)
                throw new IllegalArgumentException(INCORRECT_NAME);
            if ((submittedtask != PolishParlimentTasks.ExpensesSum && submittedtask != PolishParlimentTasks.MinorExpensesSum) && arguments.length > 2)
                throw new IllegalArgumentException(INCORRECT_NUMBER_OF_ARGUMENTS);

            int term = Integer.parseInt(arguments[arguments.length - 1]);
            if (arguments.length == 4)
                return new InputArguments(arguments[1] + " " + arguments[2], term, submittedtask);
            else
                return new InputArguments(null, term, submittedtask);
        }

    }

    private static boolean isPositive(String argument) {
        try {
            int number = Integer.parseInt(argument);
            return number > 0;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
