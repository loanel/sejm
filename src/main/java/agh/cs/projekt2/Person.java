package agh.cs.projekt2;

import java.util.List;

/**
 * Created by macho_000 on 11.01.2017.
 */
public interface Person {

    int getID();
    String getFullName();

    double getExpensesSum(String reason);
    List<Trip> getTrips();
    int getTripsTotalTime();
    Trip getTopCostTrip();
    boolean hasVisited(String country);
    void setExpenses(List<Expense> expenses);
    void setTrips(List<Trip> trips);

    String toString();

}
