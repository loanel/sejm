package agh.cs.projekt2;

/**
 * Created by macho_000 on 11.01.2017.
 */
public interface Expense {
    double getPrice();
    String getPurpose();
}
