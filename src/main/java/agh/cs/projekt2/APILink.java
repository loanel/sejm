package agh.cs.projekt2;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class APILink implements APIConnector {

    private static Gson gson = new Gson();
    private static String apiMainUrl = "https://api-v3.mojepanstwo.pl/dane/poslowie";
    private static String extension = ".json";
    private static String layerToken = "?layers[]=";
    private static String termToken = "?conditions[poslowie.kadencja]=";
    private static String expensesToken = "wydatki";
    private static String tripsToken = "wyjazdy";

    @Override
    public List<Person> getDeputies(int termNumber) throws IOException {
        String jsonUrl = apiMainUrl + extension + termToken + termNumber;
        List<Person> mpList = new LinkedList<>();
        while (jsonUrl != null) {
            JsonObject currentPage = getJsonFromUrl(jsonUrl);
            List<LinkedTreeMap<String, LinkedTreeMap>> mpJsons = gson.fromJson(currentPage.get("Dataobject"), LinkedList.class);
            for (LinkedTreeMap map : mpJsons) {
                mpList.add(new PolishParlimentMP(map));
            }
            LinkedTreeMap<String, String> links = gson.fromJson(currentPage.get("Links"), LinkedTreeMap.class);
            jsonUrl = links.get("next");
        }
        return mpList;
    }

    @Override
    public List<Expense> getExpenses(int id) throws IOException {
        List<Expense> expenses = new LinkedList<>();
        LinkedTreeMap<String, LinkedTreeMap> layers = getMPJsonLayer(id, expensesToken);
        LinkedTreeMap<String, ArrayList> expensesMap = layers.get(expensesToken);
        if (expensesMap != null) {
            ArrayList<LinkedTreeMap<String, String>> reasons = expensesMap.get("punkty");
            ArrayList<LinkedTreeMap<String, LinkedTreeMap>> years = expensesMap.get("roczniki");
            for (LinkedTreeMap year : years) {
                ArrayList<String> yearFields = (ArrayList<String>) year.get("pola");
                for (int i = 0; i < yearFields.size(); i++) {
                    double cost = Double.parseDouble(yearFields.get(i));
                    String reason;
                    if (i < 20)
                        reason = reasons.get(i).get("tytul");
                    else
                        reason = "Others/no reason";
                    expenses.add(new PolishMPExpense(cost, reason));
                }
            }
        }
        return expenses;
    }

    @Override
    public List<Trip> getTrips(int id) throws IOException {
        LinkedTreeMap layers = getMPJsonLayer(id, tripsToken);
        // equivalent to layers.get(tripsToken).toString.equals("{}") - this happens when there are no trips for PolishParlimentMP
        if (layers.get(tripsToken) instanceof LinkedTreeMap)
            return new LinkedList<>();
        else {
            List<Trip> trips = new LinkedList<>();
            ArrayList<LinkedTreeMap> tripsList = (ArrayList<LinkedTreeMap>) layers.get(tripsToken);
            for (LinkedTreeMap<String, String> trip : tripsList) {
                String destination = trip.get("kraj");
                LocalDate start = LocalDate.parse(trip.get("od"));
                LocalDate end = LocalDate.parse(trip.get("do"));
                int duration = Integer.parseInt(trip.get("liczba_dni"));
                double cost = Double.parseDouble(trip.get("koszt_suma"));
                trips.add(new PolishMPTrip(destination, start, end, duration, cost));
            }
            return trips;
        }
    }

    private LinkedTreeMap getMPJsonLayer(int id, String layer) throws IOException {
        String jsonUrl = apiMainUrl + "/" + id + extension + layerToken + layer;
        JsonObject mpJsonPage = getJsonFromUrl(jsonUrl);
        return (LinkedTreeMap<String, LinkedTreeMap>) gson.fromJson(mpJsonPage.get("layers"), LinkedTreeMap.class);
    }

    private JsonObject getJsonFromUrl(String jsonUrl) throws IOException {
        BufferedReader reader = null;
        try {
            URL url = new URL(jsonUrl);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[4096];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);
            return (JsonObject) new JsonParser().parse(buffer.toString());
        } catch (MalformedURLException ex) {
            throw new IOException(ex);
        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
