package agh.cs.projekt2;

import java.io.IOException;
import java.util.List;

/**
 * Created by macho_000 on 11.01.2017.
 */
public interface APIConnector {
    List<Person> getDeputies(int term) throws IOException;
    List<Expense> getExpenses(int deputyID) throws IOException;
    List<Trip> getTrips(int deputyID) throws IOException;
}
